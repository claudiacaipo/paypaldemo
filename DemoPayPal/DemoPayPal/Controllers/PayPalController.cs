﻿using System;
using System.Configuration;
using System.Web.Mvc;
using DemoPayPal.Models;

namespace DemoPayPal.Controllers
{
    public class PayPalController : Controller
    {
        public ActionResult RedireccionarDesdePayPal()
        {
            return View();
        }

        public ActionResult CancelarDesdePayPal()
        {
            return View();
        }

        public ActionResult NotificarDesdePayPal()
        {
            return View();
        }
        
        public ActionResult RealizarCompra(string producto, string totalPrecio)
        {
            bool useSandbox = Convert.ToBoolean(ConfigurationManager.AppSettings["IsSandbox"]);
            var paypal = new PayPalModel(useSandbox);

            paypal.item_name = producto;
            paypal.amount = totalPrecio;
            return View(paypal);
        }
    }
}
