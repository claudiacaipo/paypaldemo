﻿
using System.Configuration;

namespace DemoPayPal.Models
{
    public class PayPalModel
    {
        public string cmd { set; get; }
        public string business { set; get; }
        public string no_shipping { set; get; }
        public string @return { set; get; }
        public string cancel_return { set; get; }
        public string notify_url { set; get; }
        public string currency_code { set; get; }
        public string item_name { set; get; }
        public string amount { set; get; }
        public string actionURL { set; get; }

        public PayPalModel(bool useSandbox)
        {
            cmd = "_xclick";
            business = ConfigurationManager.AppSettings["business"];
            cancel_return = ConfigurationManager.AppSettings["cancel_return"];
            @return = ConfigurationManager.AppSettings["return"];
            actionURL = useSandbox ? ConfigurationManager.AppSettings["test_url"] : ConfigurationManager.AppSettings["Prod_url"];
            notify_url = ConfigurationManager.AppSettings["notify_url"];
            currency_code = ConfigurationManager.AppSettings["currency_code"];
        }
    }
}